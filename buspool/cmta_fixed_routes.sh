#!/bin/sh

if [ ! -f cmta_fixed_routes.zip ]; then
    curl 'https://data.texas.gov/api/file_data/qdk5yXjL3qA4Rjhjb8xTdw3iz-NbYFnH0yhOHMrmsbY?filename=cmta_fixed_routes.zip' -o cmta_fixed_routes.zip
fi

if [ ! -d cmta_fixed_routes ]; then
    unzip cmta_fixed_routes.zip -d cmta_fixed_routes
fi

shp2pgsql -d -s 2277 cmta_fixed_routes/cmta_fixed_routes.shp route | psql buspool
