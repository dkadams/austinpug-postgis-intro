#!/bin/sh

if [ ! -f pools_2013.zip ]; then
    curl -O ftp://ftp.ci.austin.tx.us/GIS-Data/Regional/regional/pools_2013.zip
fi

if [ ! -d pools_2013 ]; then
    unzip pools_2013.zip -d pools_2013
fi

shp2pgsql -dI -s 2277 pools_2013/pools_2013.shp pool | psql buspool
