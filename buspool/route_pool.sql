CREATE TABLE route_pool AS
SELECT line_id, (SELECT COUNT(*) FROM pool WHERE st_DWithin(route.geom, pool.geom, 100)) AS pool_count
FROM route;

SELECT line_name, pool_count
FROM route
NATURAL JOIN route_pool
ORDER BY pool_count DESC
LIMIT 10;

SELECT line_name, pool_count
FROM route
NATURAL JOIN route_pool
ORDER BY pool_count ASC
LIMIT 10;
