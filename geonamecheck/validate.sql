-- Naive Match
SELECT g.name, g.state, st_intersects(s.border, g.loc)
FROM geoname_us g
JOIN us_state s
  ON g.state = s.stusps
LIMIT 1000;

SELECT st_intersects(s.border, g.loc), COUNT(*)
FROM geoname_us g
JOIN us_state s
  ON g.state = s.stusps
GROUP BY 1;
