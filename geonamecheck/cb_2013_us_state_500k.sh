#!/bin/sh

if [ ! -f cb_2013_us_state_500k.zip ]; then
    curl -O http://www2.census.gov/geo/tiger/GENZ2013/cb_2013_us_state_500k.zip
fi

if [ ! -d cb_2013_us_state_500k ]; then
    unzip cb_2013_us_state_500k.zip -d cb_2013_us_state_500k
fi

shp2pgsql -d -G -g border cb_2013_us_state_500k/cb_2013_us_state_500k.shp us_state | psql geonamecheck
