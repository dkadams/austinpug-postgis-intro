#!/bin/sh

if [ ! -f allCountries.zip ]; then
    curl -O http://download.geonames.org/export/dump/allCountries.zip
fi

psql geonamecheck <<EOF
BEGIN WORK;

-- Load the raw geoname data.
DELETE FROM geoname_raw;
\COPY geoname_raw(geonameid, name, asciiname, alternatenames, latitude, longitude, feature_class, feature_code, country_code, cc2, admin1_code, admin2_code, admin3_code, admin4_code, population, elevation, dem, timezone, last_mod) FROM PROGRAM 'unzip -p allCountries.zip'  WITH (NULL '', DELIMITER E'\t')

-- Populate a table with US data
DELETE FROM geoname_us;
INSERT INTO geoname_us(geonameid, name, country_code, state, loc)
SELECT geonameid, name, country_code, admin1_code, st_SetSRID(st_MakePoint(longitude, latitude), 4326)
FROM geoname_raw
WHERE (latitude, longitude) IS NOT NULL
  AND country_code = 'US';

COMMIT;

VACUUM ANALYZE geoname_us;
VACUUM ANALYZE geoname_raw;
EOF
