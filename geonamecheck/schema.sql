CREATE EXTENSION postgis;

CREATE TABLE geoname_raw (
    geonameid INTEGER PRIMARY KEY,
    name TEXT,
    asciiname TEXT,
    alternatenames TEXT,
    latitude DOUBLE PRECISION,
    longitude DOUBLE PRECISION,
    feature_class TEXT,
    feature_code TEXT,
    country_code VARCHAR(2),
    cc2 TEXT,
    admin1_code TEXT,
    admin2_code TEXT,
    admin3_code TEXT,
    admin4_code TEXT,
    population BIGINT,
    elevation INTEGER,
    dem INTEGER,
    timezone TEXT,
    last_mod DATE
);

CREATE TABLE geoname_us (
    geonameid INTEGER PRIMARY KEY,
    name TEXT,
    country_code VARCHAR(2),
    state TEXT,
    loc GEOGRAPHY(POINT, 4326)
);

