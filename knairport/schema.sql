BEGIN WORK;

CREATE EXTENSION POSTGIS;

CREATE TYPE AIRPORT_TYPE 
AS ENUM (
    'small_airport',
    'medium_airport',
    'large_airport',
    'heliport',
    'seaplane_base',
    'balloonport',
    'closed'
);

CREATE TABLE airport (
    id INTEGER PRIMARY KEY,
    ident TEXT NOT NULL,
    type AIRPORT_TYPE NOT NULL,
    name TEXT NOT NULL,
    latitude_deg DOUBLE PRECISION,
    longitude_deg DOUBLE PRECISION,
    elevation_ft INTEGER,
    continent VARCHAR(2) NOT NULL,
    iso_country VARCHAR(2) NOT NULL,
    iso_region TEXT NOT NULL,
    municipality TEXT,
    scheduled_service BOOLEAN,
    gps_code TEXT,
    iata_code TEXT,
    local_code TEXT,
    home_link TEXT,
    wikipedia_link TEXT,
    keywords TEXT,
    loc GEOMETRY(POINT, 4326)
);

CREATE INDEX ON airport USING gist(loc);

COMMIT;
