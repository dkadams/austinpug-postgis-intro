WITH candidate(id, type, name, loc) AS (
    SELECT id, type, name, loc
    FROM airport
    ORDER BY loc <-> st_SetSRID(st_MakePoint(-97.800186, 30.348159), 4326)
    LIMIT 100
)
SELECT id, type, name, st_Distance(loc::geography, st_SetSRID(st_MakePoint(-97.800186, 30.348159), 4326)::geography) AS distance_m
FROM candidate
ORDER BY 4
LIMIT 10;
