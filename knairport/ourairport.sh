#!/bin/sh

if [ ! -f airports.csv ]; then
    curl -O http://ourairports.com/data/airports.csv
fi

psql knairport <<EOF
BEGIN WORK;

-- Load the airport data.
DELETE FROM airport;
\COPY airport(id, ident, type, name, latitude_deg, longitude_deg, elevation_ft, continent, iso_country, iso_region, municipality, scheduled_service, gps_code, iata_code, local_code, home_link, wikipedia_link, keywords) FROM 'airports.csv' WITH (FORMAT csv, HEADER);

UPDATE airport
SET loc = st_SetSRID(st_MakePoint(longitude_deg, latitude_deg), 4326)
WHERE (latitude_deg, longitude_deg) IS NOT NULL;

COMMIT;

VACUUM ANALYZE airport;
EOF
